import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';

import Product from './models/Product';

const app = express();
const router = express.Router();

app.use(cors());
app.use(bodyParser.json());

mongoose.connect('mongodb://localhost:27017/products');


const connection = mongoose.connection;

connection.once('open', () => {
    console.log('MongoDB database connection Established sucessfully!');
});

router.route('/api/product').get((req, res) => {
    Product.find((err, product) => {
        if (err)
            console.log(err);
        else 
            res.json(product);    
    });
});

router.route('/api/product/:id').get((req, res) => {
    Product.findById(req.params.id, (err, product) => {
        if (err)
            console.log(err);
        else
            res.json(product);
    });
}); 

router.route('/api/product/add').post((req, res) => {
    let product = new product(req.body);
    Product.save()
        .then(product => {
            res.status(200).json({'product': 'Added Successfully'});
        })
        .catch(err => {
            res.status(400).send('Failed to create a new record');
        })
});

router.route('/api/product/:id').post((req, res) => {
    Product.findById(req.params.id, (err, product) => {
        if (!product)
            return next(new Error('Could not load product'));
        else    
            name = req.body.name;
            brand = req.body.brand;
            quantity =  req.body.quantity;
            category = req.body.category;

        Product.save().then(product => {
            res.json('Update done');
        }).catch(err => {
            res.status(400).send('Update failed');
        });
    });
});

router.route('/api/product/:id').get((req, res) => {
    Product.findByIdAndRemove({_id: req.params.id}, (err, product) => {
        if (err) 
            res.json(err);
        else 
            res.json('Remove successfuly');    
    });
})

app.use('/', router);

app.listen(4000, () => console.log('Express server running on port 4000'));