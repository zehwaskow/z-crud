import mongoose from 'mongoose';

const Schema = mongoose.Schema;

let Product = new Schema({
    name:{
        type: String
    },
    brand: {
        type: String
        
    },
    quantity: {
        type: String
    },
    category: {
        type: String
    },
});

export default mongoose.model('Product', Product);